# 
# The default makefile for this project
# by: Isca
#
#

registry = registry.gitlab.com
project = path/to/my/project
version = 1.0.0
app = kploy

help:
	
	$(info Makefile Usage:)
	$(info build 			- build the go binary)
	$(info test 			- run unit test the code on dev)
	$(info coverage 		- run unit tests with go coverage)
	$(info citest 			- run tests on gitlab-ci environment)
	$(info install 		- install binary on OS)
	$(info uninstall 		- uninstall the binary)
	@printf "\n"

build:
	go build

test:
	@(export ProjectName=test;export Namespace=default;export Replicas=1; \
		export ContainerName=nginx:alpine;\
		export ContainerPort=80;\
		export ServicePort=80;\
		export Memory=20Mi;\
		export Cpu=100m;\
		export LimitMem=30Mi;\
		export LimitCpu=120m;\
		export Ingress=true;\
		export AppUrl=hellok8s.domain.io;\
		export AppSecret=domain-io;\
		export AppPath=/;\
		export KP_OK=true;\
		export KP_OFF_OK=false;\
		go test ./... -v -count=1;\
		go run main.go -c ./tmpl/deploy.yml -o -rm)

coverage:
	@(export ProjectName=myproject;export Namespace=someNameSpace;export Replicas=1; \
		export ContainerName=$(registry)/$(project):$(version);\
		export ContainerPort=3000;\
		export ServicePort=80;\
		export Memory=20Mi;\
		export Cpu=100m;\
		export LimitMem=30Mi;\
		export LimitCpu=120m;\
		export Ingress=true;\
		export AppUrl=mysubdomain.mydomain.com;\
		export AppSecret=my-secret;\
		export AppPath=/;\
		go test ./... -count=1 -v -cover -coverprofile .testCoverage.txt)

citest:
ifdef CI
	@(echo $(CI_COMMIT_TAG))
	@(grep gitlab.com $(KH)||ssh -o \"StrictHostKeyChecking no\" -o PasswordAuthentication=no $(SSHLOGIN)||error)
	@(docker login -u $(CIUSER) -p $(TOKEN) $(CI_REGISTRY))
else 
	$(info **** CI not setted ****)
	@printf "\r"
endif

install:
	docker run --rm --name compiler -v $(PWD):/go/src/$(app) -v /usr/local/bin:/tmp -v /etc:/opt/etc golang:1.12-alpine3.9 ash -c \
		"(cd /go/src/$(app);go build -ldflags \"-X $(app)/cmd.version=$(CI_COMMIT_TAG)\" -tags netgo -a -installsuffix cgo -o /tmp/$(app) .;mkdir -p /opt/etc/$(app);cp -vf tmpl/*.yml /opt/etc/$(app))"
uninstall: 
	rm -rvf /usr/local/bin/$(app)
	rm -rvf /etc/kploy
