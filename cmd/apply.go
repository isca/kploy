/* kploy creates k8s resources from environments to a yaml template
#########################################################
# Copyright disclaimer
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Isca disclaims all copyright interest in the program “kploy”
# (which render kubernetes templates from environment variables) is written by Igor Brandao.
# By: Isca <igorsca at protonmail dot com>
###########################################################
*/

package cmd

import (
	"bytes"
	"html/template"
	"log"
	"os"
	"os/exec"
)

// Apply the rendered file in the default k8s cluster
func (data *environs) Apply(af, rf *bool, f, k string, t *template.Template) {

	if *af {
		file, err := os.Create(f)
		if err != nil {
			log.Fatal(err)
			os.Exit(2)
		}
		t.Execute(file, data)
		file.Close()
		log.Printf("Created file %+v! Ready to deploy.", f)
		cmd := exec.Command("kubectl", "apply", "--kubeconfig", k, "-f", f)
		var outBuf, errBuf bytes.Buffer
		cmd.Stdout = &outBuf
		cmd.Stderr = &errBuf
		cmd.Run()
		if errBuf.String() != "" {
			log.Fatal(errBuf.String())
			os.Exit(2)
		}
		log.Printf(outBuf.String())

		err = rmFile(f, *rf)
		if err != nil {
			log.Fatalf("failed to remove file %+v", err)
			os.Exit(2)
		}
	}
}
