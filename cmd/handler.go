/* kploy creates k8s resources from environments to a yaml template
#########################################################
# Copyright disclaimer
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Isca disclaims all copyright interest in the program “kploy”
# (which render kubernetes templates from environment variables) is written by Igor Brandao.
# By: Isca <igorsca at protonmail dot com>
###########################################################
*/

package cmd

import (
	"fmt"
	"html/template"
	"os"
	"strings"
)

// chkVar test if the received value is not empty unless if the key is Ingress
// Ingress is a optional key and it could be empty.
func chkVar(k, v string) (b bool) {
	if v == "" && k != "Ingress" {
		return false
	}
	return true
}

// Render prints to stdout the rendered template
func (data *environs) Render(out bool, t *template.Template) {
	if out {
		t.Execute(os.Stdout, data)
	}
}

// statFile verify if received template file exists
func statFile(cfg string) (err error) {
	if _, err := os.Stat(cfg); err != nil {
		fmt.Printf("failed, use -h to list options %s\r\n", err)
		return err
		os.Exit(2)
	}
	return
}

// hostAliasRender creates hostAliases from a received variable.
// expected variable in pattern KHOST="hostA.com>1.1.1.1:hostB.com,hostC.com>1.1.1.2"
// it will split first in ":" to recognize aliases, after in ">" to split names from ip's
// and after all will split again in "," to create a slice of host.Names
func (data *environs) hostAliasRender(v string) (d *environs) {
	h := host{}
	multiHosts := strings.Split(v, ":")
	for _, mHV := range multiHosts {
		singleHost := strings.Split(mHV, ">")
		h.Ip = singleHost[1]
		singleName := strings.Split(singleHost[0], ",")
		h.Name = singleName
		data.Hosts = append(data.Hosts, h)
	}
	return
}
