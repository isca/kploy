package cmd

import (
	"html/template"
	"testing"
)

func TestChkVar(t *testing.T) {
	tt := []struct {
		k, v string
	}{
		{"batata", "abcd"},
		{"Ingress", "true"},
		{"Ingress", "false"},
		{"Value", "1"},
		{"", "batata"},
	}
	for _, tc := range tt {
		if ok := chkVar(tc.k, tc.v); !ok {
			t.Errorf("chkVar failed want: %s, got: %s", tc.v, tt[0].v)
		}
	}
}

func TestRender(t *testing.T) {
	tt := &environs{
		ProjectName: "teste",
	}
	out := true
	cfg := "../tmpl/deploy.yml"
	tmpl := template.Must(template.ParseFiles(cfg))
	tt.Render(out, tmpl)
}

func TestStatFile(t *testing.T) {
	tt := []struct {
		cfg string
	}{
		{"../tmpl/deploy.yml"},
	}
	for _, tc := range tt {
		if err := statFile(tc.cfg); err != nil {
			t.Errorf("statFile failed want: %s", tc.cfg)
		}
	}
}

func TestHostAliasRender(t *testing.T) {
	data := &environs{
		ProjectName: "test",
	}
	tt := []struct {
		value string
	}{
		{"hostA.com>1.1.1.1"},
	}

	for _, tc := range tt {
		data.hostAliasRender(tc.value)
		if data.Hosts[0].Ip == "" {
			t.Errorf("failed to create host alias, invalid pattern to convert")
		}
	}
}
