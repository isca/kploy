/* kploy creates k8s resources from environments to a yaml template
#########################################################
# Copyright disclaimer
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Isca disclaims all copyright interest in the program “kploy”
# (which render kubernetes templates from environment variables) is written by Igor Brandao.
# By: Isca <igorsca at protonmail dot com>
###########################################################
*/

package cmd

import (
	"log"
	"os/exec"
)

// RmFile remove the rendered template from OS
func rmFile(f string, rm bool) (err error) {
	if rm {
		cmd := exec.Command("rm", "-f", f)
		err = cmd.Run()
		if err != nil {
			return err
		}
	}
	log.Printf("removed %+v", f)
	return nil
}
