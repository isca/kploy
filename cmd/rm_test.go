package cmd

import "testing"

func TestRmFile(t *testing.T) {
	tt := []struct {
		in string
		vb bool
	}{
		{"batata", true},
		{"batata", false},
	}
	for _, tc := range tt {
		err := rmFile(tc.in, tc.vb)
		if err != nil {
			t.Errorf("failed %+v", err)
		}

	}

}
