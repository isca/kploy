/* kploy creates k8s resources from environments to a yaml template
#########################################################
# Copyright disclaimer
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Isca disclaims all copyright interest in the program “kploy”
# (which render kubernetes templates from environment variables) is written by Igor Brandao.
# By: Isca <igorsca at protonmail dot com>
###########################################################
*/

package cmd

import (
	"flag"
	"html/template"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// environ binds custom variables.
type environ struct {
	Name  string
	Value string
}

// host declaration will be used as hostAliases in template.
type host struct {
	Name []string
	Ip   string
}

// environs binds all supported variables to kploy.
type environs struct {
	ProjectName   string
	Namespace     string
	Replicas      string
	ContainerName string
	ContainerPort string
	ServicePort   string
	Memory        string
	Cpu           string
	LimitMem      string
	LimitCpu      string
	Ingress       string
	AppUrl        string
	AppSecret     string
	AppPath       string
	KpVal         bool
	KpHost        bool
	Values        []environ
	Hosts         []host
}

// flags initialize all arguments flags.
func flags() (vf, outCfg, rm, apply, ign *bool, cfg, kubeconfig string) {

	vf = flag.Bool("version", false, "Print version.")
	outCfg = flag.Bool("o", false, "Print rendered template to the output.")
	rm = flag.Bool("rm", false, "Remove rendered template file after deploy.")
	apply = flag.Bool("apply", false, "Apply rendered template in the cluster.")
	ign = flag.Bool("i", true, "Ignore variable validation allowing to use empty.")
	flag.StringVar(&cfg, "c", "/etc/kploy/deploy.yml", "Specify the default template file.")
	flag.StringVar(&kubeconfig, "target", os.Getenv("HOME")+"/.kube/config", "Specify the kubeconfig file to be used.")
	flag.Parse()

	return
}

func bindVar() (data *environs) {

	data = &environs{
		ProjectName:   os.Getenv("ProjectName"),
		Namespace:     os.Getenv("Namespace"),
		Replicas:      os.Getenv("Replicas"),
		ContainerName: os.Getenv("ContainerName"),
		ContainerPort: os.Getenv("ContainerPort"),
		ServicePort:   os.Getenv("ServicePort"),
		Memory:        os.Getenv("Memory"),
		Cpu:           os.Getenv("Cpu"),
		LimitMem:      os.Getenv("LimitMem"),
		LimitCpu:      os.Getenv("LimitCpu"),
		Ingress:       os.Getenv("Ingress"),
		AppUrl:        os.Getenv("AppUrl"),
		AppSecret:     os.Getenv("AppSecret"),
		AppPath:       os.Getenv("AppPath"),
	}

	env := environ{}
	for _, vars := range os.Environ() {
		variable := strings.Split(vars, "=")
		match, _ := regexp.MatchString(`^KP_`, variable[0])
		if match {
			data.KpVal = true
			env.Name = strings.Replace(variable[0], "KP_", "", 1)
			env.Value = variable[1]
			data.Values = append(data.Values, env)
		}
		if variable[0] == "KHOST" {
			data.KpHost = true
			data.hostAliasRender(variable[1])
		}
	}

	return

}

// Run is main proccess to call and execute every commands.
func Run() {

	vf, outCfg, rm, apply, _, cfg, kubeconfig := flags()

	//run version
	if *vf {
		vshow(version)
		return
	}

	//verify template file
	statFile(cfg)

	//bind variables
	data := bindVar()

	//validate everything but ingress
	//if !*ign {
	//	for k, v := range data {
	//		if ok := chkVar(k, v); !ok {
	//			err := errors.New("failed render template " + k + " is empty ")
	//			log.Fatal(err)
	//			os.Exit(2)
	//		}
	//	}
	//}

	//rendering template
	tmpl := template.Must(template.ParseFiles(cfg))
	data.Render(*outCfg, tmpl)
	file := "/tmp/kploy-" + data.ProjectName + "-" + strconv.Itoa(int(time.Now().UnixNano())) + ".yml"
	data.Apply(apply, rm, file, kubeconfig, tmpl)

}
