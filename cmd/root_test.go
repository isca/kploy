package cmd

import (
	"os"
	"testing"
)

func TestBindVar(t *testing.T) {

	os.Setenv("ProjectName", "teste")
	data := bindVar()
	if data.ProjectName == "" {
		t.Errorf("failed to bind variables want: %s", data.ProjectName)
	}

}
